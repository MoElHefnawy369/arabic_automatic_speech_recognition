#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os, sys,io
import subprocess
from PyQt5.QtWidgets import  QInputDialog, QLineEdit, QFileDialog
from PyQt5.QtWidgets import (QWidget, QLabel,QComboBox, QApplication,QPushButton)
from PyQt5.QtCore import pyqtSlot

model = ''
Wav_dir=''
model_dir=''
class Example(QWidget):
    def __init__(self, parent=None):
        #super().__init__()
        super(Example, self).__init__(parent=parent)
        self.initUI()
        
        
    def initUI(self):      
        self.lbl = QLabel("Default", self)
        combo = QComboBox(self)
        combo.addItem("Choose Model")
        combo.addItem("TDNN")
        combo.addItem("TDNN-LSTM")
        #combo.addItem("TDNN-BLSTM")
        combo.move(100, 50)
        self.lbl.move(50, 150)
        combo.activated[str].connect(self.onActivated)
        
        button = QPushButton('Import WAV', self)
        button.setToolTip('This is an example button')
        button.move(100,100) 
        button.clicked.connect(self.SingleBrowse3)         
        self.setGeometry(400, 400, 500, 300)
        self.setWindowTitle('Import Files')
        self.show()
        
    def SingleBrowse3(self):
        #filePath = QtGui.QFileDialog.getOpenFileName(self,'Single File')
        filePath = QFileDialog.getOpenFileName(self,'Single File')
        #self.lbl.setText('Wait ')
        #self.lbl.adjustSize()
	
        global Wav_dir
        global model
        Wav_dir=str(filePath)
        Wav_dir=Wav_dir[3:-20]
        print('wav   '+Wav_dir)
        print('mmmodel  '+model)
        with open('/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/testSample/wav.scp', 'w') as wa:
            wa.write('1-50-1 ')
            wa.write(str(Wav_dir))
        print(model)
        #sys.exit()
        if(model=='TDNN'):
        
            v=subprocess.check_call('/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/decode_tdnn.sh')
            
            print(model)
            #sys.exit()
        else:
            v=subprocess.check_call('/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/decode_tdnn_lstm.sh')
        
                
        #v=subprocess.check_call(['/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/tryOnline.sh',  model, Wav_dir])
        print(v)
        #textfile=model_dir+'/online/output.txt'
        #textfile='/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/testes.txt'
        #f.run_script(Wav_dir,textfile)
        #v=subprocess.check_call(['/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/wavtext.py',Wav_dir,textfile])
        textfile=model_dir+'/decode__test_sample/log/decode.1.log'
        print(v)
        print('Done ya ')
        with open('output15min01_2nd.txt', 'w') as f:
            
            #f = io.open('textf.txt', mode="w", encoding="utf-8")
            f.write(str(Wav_dir))
            f.write(str('\n'))
            f.write(str(textfile))
        #f.close()
        #self.quit()
        
        self.close()

    @pyqtSlot()
    def on_click(self):
        print('PyQt5 button click')
        #combo.activated[str].connect(self.onActivated)
                    
    def onActivated(self, text):
        global model
        global model_dir
        model= text
        if(text=='TDNN'):
            print('ttttt '+text)
            model_dir='/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/exp/nnet3/tdnn_new_model_90train7'
        elif(text=='TDNN-LSTM'):
            print('llllss '+text)
            model_dir='/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/exp/nnet3/tdnn_lstm1a_sp'
        self.lbl.setText('Importing Model and Modifying Wav frequency')
        self.lbl.adjustSize()  
        
def main():               
#if __name__ == '__main__': 
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
    print('finish')
    global Wav_dir
    #return(Wav_dir)
    #global Wav_dir
    #textfile='/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/testes.txt'
    #f.run_script(Wav_dir,textfile)
    
main()
