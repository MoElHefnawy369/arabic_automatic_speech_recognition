#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import soundfile as sf
import time
import io
import pyaudio  
import wave
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from nltk.tokenize import word_tokenize
from multiprocessing import Process
import os,re,sys
import csv

reload(sys)  
sys.setdefaultencoding('utf-8')

tokens = []
pos = -1
wavef=''
class App(QWidget):
    glob = 'init'
    globC = 0
    def __init__(self, parent=None):
        super(App, self).__init__(parent=parent)  # these values change where the main window is placed
        self.title = 'Arabic Speech Recognition Demo'
        self.left = 40
        self.top = 40
        self.width = 800
        self.height = 500
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # call the gridlayout function
        self.createGridLayout()
        self.time_label.text = 'Hello '
        windowLayout = QVBoxLayout()
        windowLayout.addWidget(self.horizontalGroupBox)
        self.setLayout(windowLayout)
        self.show()  # this sets the main window to the screen size

    def createGridLayout(self):
        tim = self.getTime()
        self.time_label = QLabel(tim, self)
        self.horizontalGroupBox = QGroupBox()
        layout = QGridLayout()
        layout.addWidget(self.time_label, 0, 2)
        self.horizontalGroupBox.setLayout(layout)

    def getTime(self):
	global glob
	global globC
	globC = 0
        tim = ''
	glob = tim
        return tim

    def updateTime(self):
	global glob, globC
	global tokens, pos
	globC = globC + 1
	pos = pos + 1
	if pos == len(tokens):
	    time.sleep(20)
	    sys.exit()
	if globC == 18:
	    globC = 0
	    glob = glob + '\n'	
	glob = glob + ' ' + tokens[pos]
        tim = glob
        #print("Time: " + time)
        self.time_label.setText(tim)
        return tim

def main(wavef,textf):
    f = sf.SoundFile(wavef)
    secs = float(len(f)) / float(f.samplerate) + 0.5
    
    f = io.open(textf, mode="r", encoding="utf-8")
    text = f.read()
    
    global tokens
    tokens = word_tokenize(text)
    dell = (float(secs) / float(1+len(tokens))) * 1000
    print('Delay = ' + str(dell))
    app = QApplication(sys.argv)
    ex = App()
    
    timer = QTimer()
    timer.timeout.connect(ex.updateTime)
    timer.start(dell)

    sys.exit(app.exec_())
textfile=''
wavfile=''

def matchSpeaker(line):
    matchThis = ""
    matched = re.match(r'\d-\d\d-\d',line)
    if matched:
        #matches a date and adds it to matchThis
        matchThis = matched.group() 
    else:
        matchThis = "NONE"
    return matchThis    

def generateDicts(log_fh):
    with open('conv.csv', 'rb') as csvfile:
        mapping = csv.reader(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
        with open('outputresult.txt', 'w') as f:
            mapping = csv.reader(csvfile, delimiter=',', quotechar='|')          
            mapp = {}
            mapp2 = {}
            for row in mapping:
                letterE = row[0]
                letterA = row[1]
                mapp[letterA] = letterE
                mapp2[letterE] = letterA
            currentDict = {}
            for line in log_fh:
                #print('---------------------------------------------------------------- ')
                #print('mis   '+line)
                
                if line.startswith(matchSpeaker(line)):
                    print('matched  '+line)
                    line =line.split("-")[-1]
                    line = line[2:-1]
                    #print(line)
                    #sys.exit()
                    txt2 = ''
                    for i in line:
                        if i == ' ':
                            txt2 = txt2 + ' '
                        else:
                            cc = mapp2[i]
                            txt2 = txt2 + cc
                    #print(txt2)
                    #print('the line is    '+line)
                    f.write(str(txt2))
                    f.write('\n')
                    #f.write('---------------------------------------------- \n')
                    if currentDict:
                        yield currentDict
                    currentDict = {"Speaker":line.split("-")[0][:5]}
                #else:
            #    currentDict += str(line)
            
    yield currentDict

finaltext='output_result.txt'
"""
def generateDicts(log_fh):
    with open('conv.csv', 'rb') as csvfile:
        mapping = csv.reader(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
        with open(finaltext, 'w') as f:
            mapping = csv.reader(csvfile, delimiter=',', quotechar='|')          
            mapp = {}
            mapp2 = {}
            for row in mapping:
                letterE = row[0]
                letterA = row[1]
                mapp[letterA] = letterE
                mapp2[letterE] = letterA
            currentDict = {}
            for line in log_fh:
                if line.startswith('utterance-id1 '):
                    ##########line =line.split("-")[-1]
                    line = line[13:-1]
                    #print(line)
                    #sys.exit()
                    txt2 = ''
                    for i in line:
			
                        if i == ' ':
                            txt2 = txt2 + ' '
                        else:

                            cc = mapp2[i]
                            txt2 = txt2 + cc
                    #print(txt2)
                    #print('the line is    '+line)
                    f.write(str(txt2))
                    #f.write('\n')
                    #f.write('---------------------------------------------- \n')
                    if currentDict:
                        yield currentDict
                    currentDict = {"Speaker":line.split("-")[0][:5]}
                #else:
            #    currentDict += str(line)
    yield currentDict
"""
def run_script():
    global wavfile
    global textfile
    n=0
    with open('output15min01_2nd.txt', 'r') as f:
        for i in f:
            #print(f)
            if(n==0):
                wavfile=i[:-1]
                print('ssss '+wavfile)
            else:
                textfile=i
                print('txt is '+textfile)
            n+=1
        
    with open(textfile) as f:
        listNew= list(generateDicts(f))
    #print(wavfile)    
    #print(textfile) 
    global finaltext
    finaltext= 'outputresult.txt'
    print(finaltext)  
    print('entereee ')
    
    #finaltext=
    #wavfile,textfile

    #global wavef
    #wavfile='/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/1-30-1.wav'
#if __name__ == '__main__':
    p = Process(target=main,args=(wavfile,finaltext))
    p.start()
    #define stream chunk   
    chunk = 1024  
    #open a wav format music  
    f = wave.open(wavfile,"rb")  
    #instantiate PyAudio  
    p2 = pyaudio.PyAudio()  
    #open stream  
    stream = p2.open(format = p2.get_format_from_width(f.getsampwidth()),  
                    channels = f.getnchannels(),  
                    rate = f.getframerate(),  
                    output = True)  
    #read data  
    data = f.readframes(chunk)  
    #play stream  
    while data:
	stream.write(data)
	data = f.readframes(chunk)
    p.join()
    #stop stream  
    stream.stop_stream()  
    stream.close()  
    #close PyAudio  
    p2.terminate()

run_script()
