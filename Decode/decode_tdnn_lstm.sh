#!/bin/bash
. ./cmd.sh
. ./path.sh 
. ./utils/parse_options.sh


if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi
run.sh || exit 1;
dir=exp/nnet3/tdnn_lstm1a_sp
#dir=$1
ali_dir=exp/tri3_ali
train_stage=-10
speed_perturb=false
remove_egs=true
graph_dir=exp/tri3/graph
frames_per_chunk=
chunk_width=40,30,20
chunk_left_context=40
chunk_right_context=0
remove_egs=false
stage=15
num_jobs=1
test_folder=testSample
#test_folder=/home/hagrass/kaldi/egs/RDI_CHALLENGE/try-1/Training/testSample
#test_folder=$2
#test_folder=train_cv10

for data in $test_folder; do
    (
      frames_per_chunk=$(echo $chunk_width | cut -d, -f1)
      data_affix=$(echo $data | sed s/test_//)
      nj=$(wc -l <${data}/spk2utt)
      #for lmtype in tgpr; do
      steps/nnet3/decode2.sh \
        --extra-left-context $chunk_left_context \
        --extra-right-context $chunk_right_context \
        --extra-left-context-initial 0 \
        --extra-right-context-final 0 \
        --frames-per-chunk $frames_per_chunk \
        --nj $nj --cmd "$decode_cmd"  --num-threads 4 \
        $graph_dir ${data} $dir/decode__test_sample || exit 1
	) &
  done
      #done
    #) || touch $dir/.error &
  #done
  #wait
  #[ -f $dir/.error ] && echo "$0: there was a problem while decoding" && exit 1
wait;
exit 0;
