#!/bin/bash

# tdnn_blstm_1d is same as tdnn_blstm_1c, but with the perframe-dropout added

# ./local/chain/compare_wer_general.sh tdnn_blstm_1c_sp tdnn_blstm_1d_sp
# System                tdnn_blstm_1c_sp tdnn_blstm_1d_sp
# WER on train_dev(tg)      12.88     12.61
# WER on train_dev(fg)      12.01     11.61
# WER on eval2000(tg)        15.6      15.1
# WER on eval2000(fg)        14.2      13.7
# Final train prob         -0.060    -0.069
# Final valid prob         -0.085    -0.090
# Final train prob (xent)        -0.737    -0.806
# Final valid prob (xent)       -0.8558   -0.8922

set -e

# configs for 'chain'
stage=12
train_stage=-10
get_egs_stage=-10
speed_perturb=true
dir=exp/chain/tdnn_blstm_1d_03.06.2018_1030PM  # Note: _sp will get added to this if $speed_perturb == true.
decode_iter=
decode_dir_affix=

# training options
chunk_width=140,100,160
chunk_left_context=20
chunk_right_context=20
xent_regularize=0.025
self_repair_scale=0.00001
label_delay=0
dropout_schedule='0,0@0.20,0.1@0.50,0'
# decode options
frames_per_chunk=
remove_egs=false
common_egs_dir=

affix=
# End configuration section.
echo "$0 $@"  # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

# The iVector-extraction and feature-dumping parts are the same as the standard
# nnet3 setup, and you can skip them by setting "--stage 8" if you have already
# run those things.

suffix=
if [ "$speed_perturb" == "true" ]; then
  suffix=_sp
fi

dir=$dir${affix:+_$affix}
if [ $label_delay -gt 0 ]; then dir=${dir}_ld$label_delay; fi
dir=${dir}$suffix
train_set=train_tr90
ali_dir=exp/tri3_ali
#treedir=exp/chain/tri5_7d_tree$suffix
lang=data/lang





if [ $stage -le 9 ]; then
  # Get the alignments as lattices (gives the CTC training more freedom).
  # use the same num-jobs as the alignments
  nj=$(cat exp/tri3_ali/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$train_cmd" data/$train_set \
    data/lang exp/tri3 exp/tri3_lats_nodup$suffix
  rm exp/tri3_lats_nodup$suffix/fsts.*.gz # save space
fi


if [ $stage -le 10 ]; then
  # Create a version of the lang/ directory that has one state per phone in the
  # topo file. [note, it really has two states.. the first one is only repeated
  # once, the second one has zero or more repeats.]
  rm -rf $lang
  cp -r data/lang $lang
  silphonelist=$(cat $lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  #steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$lang/topo
fi


#if [ $stage -le 11 ]; then
#  # Build a tree using our new topology.
#  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
#      --context-opts "--context-width=2 --central-position=1" \
#      --cmd "$train_cmd" 7000 data/$train_set $lang $ali_dir $treedir
#fi

if [ $stage -le 12 ]; then
  echo "$0: creating neural net configs using the xconfig parser";

 # num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
  num_targets=$(tree-info $ali_dir/tree |grep num-pdfs|awk '{print $2}')
  [ -z $num_targets ] && { echo "$0: error getting num-targets"; exit 1; }
  learning_rate_factor=$(echo "print 0.5/$xent_regularize" | python)

  lstm_opts="decay-time=20 dropout-proportion=0.0"

  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  #input dim=100 name=ivector
  input dim=40 name=input

  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-2,-1,0,1,2) affine-transform-file=$dir/configs/lda.mat

  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=1024 #520
  relu-renorm-layer name=tdnn2 input=Append(-1,0,1) dim=1024 #520
#  relu-renorm-layer name=tdnn3 input=Append(-1,0,1) dim=520 #520
  relu-renorm-layer name=tdnn3 input=Append(-2,0,2) dim=1024 #520 new one
  # *************************************************
  # replace each 520 with 520
  # replace each 356 with 520
  # replace each 656 with 520
  # *******************************************
  # check steps/libs/nnet3/xconfig/lstm.py for the other options and defaults

  # recurrent-projection-dim=260 non-recurrent-projection-dim=260
  fast-lstmp-layer name=blstm1-forward input=tdnn3 cell-dim=1024 recurrent-projection-dim=260 non-recurrent-projection-dim=260 delay=-3 $lstm_opts
  fast-lstmp-layer name=blstm1-backward input=tdnn3 cell-dim=1024 recurrent-projection-dim=260 non-recurrent-projection-dim=260 delay=3 $lstm_opts

  fast-lstmp-layer name=blstm2-forward input=Append(blstm1-forward, blstm1-backward) cell-dim=1024 recurrent-projection-dim=260 non-recurrent-projection-dim=260 delay=-3 $lstm_opts
  fast-lstmp-layer name=blstm2-backward input=Append(blstm1-forward, blstm1-backward) cell-dim=1024 recurrent-projection-dim=260 non-recurrent-projection-dim=260 delay=3 $lstm_opts

  fast-lstmp-layer name=blstm3-forward input=Append(blstm2-forward, blstm2-backward) cell-dim=1024 recurrent-projection-dim=260 non-recurrent-projection-dim=260 delay=-3 $lstm_opts
  fast-lstmp-layer name=blstm3-backward input=Append(blstm2-forward, blstm2-backward) cell-dim=1024 recurrent-projection-dim=260 non-recurrent-projection-dim=260 delay=3 $lstm_opts

#  ## adding the layers for chain branch
  output-layer name=output input=Append(blstm3-forward, blstm3-backward) output-delay=$label_delay dim=$num_targets max-change=1.5
  # adding the layers for xent branch
  # This block prints the configs for a separate output that will be
  # trained with a cross-entropy objective in the 'chain' models... this
  # has the effect of regularizing the hidden parts of the model.  we use
  # 0.5 / args.xent_regularize as the learning rate factor- the factor of
  # 0.5 / args.xent_regularize is suitable as it means the xent
  # final-layer learns at a rate independent of the regularization
  # constant; and the 0.5 was tuned so as to make the relative progress
  # similar in the xent and regular final layers.
  

EOF

 
  steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs

fi

if [ $stage -le 13 ]; then
  if [[ $(hostname -f) == *.clsp.jhu.edu ]] && [ ! -d $dir/egs/storage ]; then
    utils/create_split_dir.pl \
     /export/b0{5,6,7,8}/$USER/kaldi-data/egs/swbd-$(date +'%m_%d_%H_%M')/s5c/$dir/egs/storage $dir/egs/storage
  fi
#    --trainer.samples-per-iter 10000 \
#    --trainer.rnn.num-chunk-per-minibatch 64 \
#    --trainer.optimization.initial-effective-lrate 0.001 \
#    --trainer.optimization.final-effective-lrate 0.0001 \

  steps/nnet3/train_rnn.py --stage $train_stage \
    --cmd "$decode_cmd" \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --trainer.rnn.num-chunk-per-minibatch 32 \
    --trainer.samples-per-iter 10000 \
    --trainer.max-param-change 2.0 \
    --trainer.num-epochs 4 \
    --trainer.optimization.shrink-value 0.99 \
    --trainer.optimization.num-jobs-initial 1 \
    --trainer.optimization.num-jobs-final 1 \
    --trainer.optimization.initial-effective-lrate 0.005 \
    --trainer.optimization.final-effective-lrate 0.00001 \
    --trainer.optimization.momentum 0.0 \
    --trainer.deriv-truncate-margin 8 \
    --egs.chunk-width $chunk_width \
    --egs.chunk-left-context $chunk_left_context \
    --egs.chunk-right-context $chunk_right_context \
    --egs.chunk-left-context-initial=0 \
    --egs.chunk-right-context-final=0 \
    --trainer.dropout-schedule $dropout_schedule \
    --egs.dir "$common_egs_dir" \
    --cleanup.remove-egs $remove_egs \
    --feat-dir ${train_set}\
    --lang=$lang \
    --ali-dir $ali_dir \
    --dir $dir  || exit 1;
fi

exit
#  STOP HERE ***************************************************************************************

decode_suff=sw1_tg
graph_dir=$dir/graph_sw1_tg
if [ $stage -le 15 ]; then
  iter_opts=
  if [ ! -z $decode_iter ]; then
    iter_opts=" --iter $decode_iter "
  fi
  for decode_set in train_dev eval2000; do
      (
      steps/nnet3/decode.sh --acwt 1.0 --post-decode-acwt 10.0 \
          --nj 50 --cmd "$decode_cmd" $iter_opts \
          --extra-left-context $chunk_left_context  \
          --extra-right-context $chunk_right_context  \
          --extra-left-context-initial 0 \
          --extra-right-context-final 0 \
          --frames-per-chunk "$frames_per_chunk" \
         $graph_dir data/${decode_set}_hires \
         $dir/decode_${decode_set}${decode_dir_affix:+_$decode_dir_affix}_${decode_suff} || exit 1;
      ) &
  done
fi
wait;
exit 0;
