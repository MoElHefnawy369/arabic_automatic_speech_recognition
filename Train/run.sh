#!/bin/bash -aux

# Copyright (C) 2017-2018, RDI

. ./cmd.sh
. ./path.sh

##########################################################
#
#  Initial notes
#
##########################################################

LEX_DIR=/home/silkbytes/kaldi/egs/RDI_CHALLENGE/try-1/lexicon

nj=1  # split training into how many jobs?
nDecodeJobs=1

##########################################################
#  Recipe
##########################################################



#1) Data preparation

#DATA PREPARATION
utils/fix_data_dir.sh testSample
#exit
#LEXICON PREPARATION: The lexicon is also provided
echo "Preparing dictionary"
local/graphgeme_rdi_prep_dict.sh $LEX_DIR
#exit
#L Compilation
echo "Preparing lang dir"
#utils/prepare_lang.sh local/dict "<UNK>" local/lang data/lang
#exit
#G compilation
#local/rdi_format_data.sh

path='/home/silkbytes/kaldi/egs/RDI_CHALLENGE/try-1/Training'
#Calculating mfcc features

echo "Computing features"

mfccdir=testSample/mfcc
steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $path/testSample exp2/make_mfcc_sampleFinal/train/log $mfccdir

steps/compute_cmvn_stats.sh $path/testSample exp2/make_mfcc_sampleFinal/train/log $mfccdir

echo "finished till here"
exit

#Monophone training
#steps/train_mono.sh --nj $nj --cmd "$train_cmd" \
 # $path/train data/lang exp/mono 

#Monophone alignment
#steps/align_si.sh --nj $nj --cmd "$train_cmd" \
  #$path/train data/lang exp/mono exp/mono_ali 

#tri1 [First triphone pass]
#steps/train_deltas.sh --cmd "$train_cmd" \
 # 2500 30000 $path/train data/lang exp/mono_ali exp/tri1 

#tri1 decoding
#utils/mkgraph.sh data/lang_test exp/tri1 exp/tri1/graph


#  steps/decode.sh --nj $nDecodeJobs --cmd "$decode_cmd" --config conf/decode.config \
#    exp/tri1/graph $path/train exp/tri1/decode

#tri1 alignment
#steps/align_si.sh --nj $nj --cmd "$train_cmd" \
 # $path/train data/lang exp/tri1 exp/tri1_ali 

#tri2 [a larger model than tri1]
#steps/train_deltas.sh --cmd "$train_cmd" \
 # 3000 40000 $path/train data/lang exp/tri1_ali exp/tri2

#tri2 decoding
#utils/mkgraph.sh data/lang_test exp/tri2 exp/tri2/graph


#steps/decode.sh --nj $nDecodeJobs --cmd "$decode_cmd" --config conf/decode.config \
# exp/tri2/graph $path/train exp/tri2/decode


#tri2 alignment
#steps/align_si.sh --nj $nj --cmd "$train_cmd" \
 # $path/train data/lang exp/tri2 exp/tri2_ali

# tri3 training [LDA+MLLT]
#steps/train_lda_mllt.sh --cmd "$train_cmd" \
  #4000 50000 $path/train data/lang exp/tri1_ali exp/tri3

#tri3 decoding
utils/mkgraph.sh data/lang_test exp/tri3 exp/tri3/graph

exit
#steps/decode.sh --nj $nDecodeJobs --cmd "$decode_cmd" --config conf/decode.config \
#exp/tri3/graph $path/train exp/tri3/decode


#tri3 alignment
steps/align_si.sh --nj $nj --cmd "$train_cmd"  $path/train data/lang exp/tri3 exp/tri3_ali

